#pragma once
#include <dx\src\api>

// E: 19

struct Symbol
{
	dx::uint type;
	dx::String name;
	dx::String value;
};

struct TemplateWrapper
{
	dx::String type_name; // e.g template<typename T> => T
	dx::uint type; // DataType_t
	dx::String after_name;
};

struct Group
{
	dx::String name;
	std::vector<Symbol> symbols_;

	Symbol *get_symbol( const dx::String &name );
	
	void add_symbol( Symbol symbol );

	dx::String get_value( const dx::String &key );
};

struct Concept
{
	struct RequiresBlock
	{
		// Arguments
		Group arguments;

		// Expression
		dx::String left, right;
		dx::String operation;
	};
	
	dx::String name;
	TemplateWrapper wrapper;
	RequiresBlock requires;
};

struct ConfigModule
{
	dx::String name;
	std::vector<Group> groups;
	std::vector<Symbol> globals;
	std::vector<ConfigModule> imports;
	std::vector<Concept> concepts;

	Symbol *get_global_symbol( const dx::String &name );
	
	void add_global_symbol( Symbol symbol );

	dx::String get_global_value( const dx::String &key );

	Group *get_group( const dx::String &name );
};

enum DataType_t
{
	data_type_string,
	data_type_int,
	data_type_float,
	data_type_double,
	data_type_char,
	data_type_template,
};

class Config
{
private:
	ConfigModule module_;
	std::vector<ConfigModule> importmods_;
	Group *group_;
	dx::Tokenizer tokenizer_;
	dx::Event<void(dx::String)> _OnMessage;
	dx::Event<void(dx::String)> _OnWarning;
	dx::Event<void(dx::String)> _OnError;

public:
	Config( );

	void set_file( dx::File file );

	bool load( );

	bool save( ) const;

	void add_possible_import_module( ConfigModule module );

	ConfigModule* get_module( );

	dx::Event<void(dx::String)> &OnMessage( );
	dx::Event<void(dx::String)> &OnWarning( );
	dx::Event<void(dx::String)> &OnError( );
private:
	void add_symbol_smart( Symbol symbol );
	bool parse_keyword( dx::Tokenizer::iterator &it, const dx::Tokenizer::iterator &end, bool &has );
	bool parse_symbol( dx::Tokenizer::iterator &it, const dx::Tokenizer::iterator &end, bool dont_semi = false );
	bool parse_symbol_concept( dx::Tokenizer::iterator &it, const dx::Tokenizer::iterator &end );
	bool parse_argument( dx::Tokenizer::iterator &it, const dx::Tokenizer::iterator &end, Group *group );
	bool is_type( dx::uint kw );
	bool is_concept( const dx::Tokenizer::iterator &it );
	bool verify_concept_block( const dx::String &concept, Symbol *symbol );
	bool check_literal_op( const dx::String &operation );
	bool check_floating_point_op( const dx::String &operation );
	bool check_string_op( const dx::String &operation );

	ConfigModule get_import( dx::String name );
	dx::String find_global_value( const dx::String &name );

	bool error( dx::String str, bool ret = false );
	bool warning( dx::String str, bool ret = true );
	bool msg( dx::String str,  bool ret = true  );
};