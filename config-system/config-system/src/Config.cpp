#include "Config.h"

Config::Config()
	: module_( ), group_( nullptr ), tokenizer_( dx::File( ) )
{ 
	ConfigModule defines_module;
	defines_module.name = "defines";
	defines_module.add_global_symbol( { data_type_int, "DEFINES_KEY_F1", "112" } );
	defines_module.add_global_symbol( { data_type_int, "DEFINES_KEY_F2", "113" } );
	defines_module.add_global_symbol( { data_type_int, "DEFINES_KEY_F3", "114" } );
	defines_module.add_global_symbol( { data_type_int, "DEFINES_KEY_F4", "115" } );
	defines_module.add_global_symbol( { data_type_int, "DEFINES_KEY_F5", "116" } );
	defines_module.add_global_symbol( { data_type_int, "DEFINES_KEY_F6", "117" } );
	defines_module.add_global_symbol( { data_type_int, "DEFINES_KEY_F7", "118" } );
	defines_module.add_global_symbol( { data_type_int, "DEFINES_KEY_F8", "119" } );
	defines_module.add_global_symbol( { data_type_int, "DEFINES_KEY_F9", "120" } );
	defines_module.add_global_symbol( { data_type_int, "DEFINES_KEY_F10", "121" } );
	defines_module.add_global_symbol( { data_type_int, "DEFINES_KEY_F11", "122" } );
	add_possible_import_module( std::move( defines_module ) );
}

void Config::set_file(dx::File file)
{
	tokenizer_.setFile( std::move( file ) );
}

bool Config::load()
{
	tokenizer_.tokenize( );

	auto it = tokenizer_.begin( ), end = tokenizer_.end( );
	bool has_module = false;
	bool in_scope = false;
	msg( "compiling..." );
	while( it < end )
	{
		if ( it->type == dx::token_keyword  )
		{
			if ( !parse_keyword( it, end, has_module ) )
				return false;
		}
		else if ( is_concept( it ) )
		{
			if ( !parse_symbol_concept( it, end ) )
				return false;

		}
		else if ( it->kw == dx::op_squigglybracket_open )
		{
			in_scope = true;
			group_ = &module_.groups.back( );
			++it;
		}
		else if ( it->kw == dx::op_squigglybracket_close )
		{
			if ( !in_scope )
				return error( "E1011: cannot close global scope, line: " + dx::to_string( it->line ) );
			in_scope = false;
			group_ = nullptr;
			++it;
		}
		else
		{
			if ( it->kw != dx::op_semicolon )
				warning( "There was an unknown token ('" + it->value + "'), line: " + dx::to_string( it->line ) );
			++it;
		}
	}
	return true;
}

bool Config::save() const
{
	return true;
}

void Config::add_possible_import_module(ConfigModule module)
{
	importmods_.emplace_back( std::move( module ) );
}

ConfigModule * Config::get_module()
{
	return &module_;
}

dx::Event<void(dx::String)>& Config::OnMessage()
{
	return _OnMessage;
}

dx::Event<void(dx::String)>& Config::OnWarning()
{
	return _OnWarning;
}

dx::Event<void(dx::String)>& Config::OnError()
{
	return _OnError;
}

void Config::add_symbol_smart(Symbol symbol)
{
	// TODO: Implement scope adding etc
	if ( group_ )
		group_->add_symbol( std::move( symbol ) );
	else
		module_.add_global_symbol( std::move( symbol ) );
}

bool Config::parse_keyword(dx::Tokenizer::iterator & it, const dx::Tokenizer::iterator & end, bool &has)
{
	if ( is_type( it->kw ) )
	{
		if ( !has )
			return error( "E1008: missing module, line:  " + dx::to_string( it->line ) );
		return parse_symbol( it, end );
	}
	else if ( it->kw == dx::kw_module )
	{
		if ( has )
			return error( "E1008: missing module, line:  " + dx::to_string( it->line ) );

		auto ident = ++it;

		if ( ident->type != dx::token_identifier )
			return error( "E1010: cannot create ghosts, expected identifier, line: " + dx::to_string( ident->line ) );

		module_.name = ident->value;
		has = true;
		++it; // eat identifier
	}
	else if ( it->kw == dx::kw_import )
	{
		if ( !has )
			return error( "E1008: missing module, line:  " + dx::to_string( it->line ) );

		auto import = ++it;

		if ( import->type != dx::token_identifier )
			return error( "E1009: cannot import ghosts, expected identifier, line: " + dx::to_string( import->line ) );

		// Eat ident and check semicolon
		if ( (++it)->kw != dx::op_semicolon )
			return error( "E1005: missing semicolon, line: " + dx::to_string( it->line ) );

		module_.imports.emplace_back( get_import( import->value ) );
		++it; // eat semiclon
	}
	else if ( it->kw == dx::kw_struct )
	{
		if ( !has )
			return error( "E1008: missing module, line:  " + dx::to_string( it->line ) );
		auto ident = ++it;

		if ( ident->type != dx::token_identifier )
			return error( "E1007: cannot create ghost structs, expected identifier, line: " + dx::to_string( ident->line ) );

		Group group;
		group.name = ident->value;

		module_.groups.emplace_back( std::move( group ) );
		++it; // Eat identifier
	}
	else if ( it->kw == dx::kw_template )
	{ // template
		
		// Used to compile the following:
		//
		//	template<typename T>
		//	concept bool Addable = requires( T a, T b ) 
		//						   {
		//							 a + b;
		//						   };
		//

		// <
		if ( (++it)->kw != dx::op_less )
			return error( "E1016: no operator< after 'template', line: " + dx::to_string( it->line ) );
		
		if ( (++it)->kw == dx::op_greater )
			return error( "E1017: Template parameter may not be empty, line: " + dx::to_string( it->line ) );
		
		// typename
		if ( it->kw != dx::kw_typename )
			return error( "E1018: Template paramater must be declared using 'typename', line: " + dx::to_string( it->line ) );

		// T
		if ( (++it)->type != dx::token_identifier )
			return error( "E1019: Template paramater must have a name, line: " + dx::to_string( it->line ) );
		
		TemplateWrapper wrapper;
		wrapper.type_name = it->value; // e.g 'T'
		
		// >
		if ( (++it)->kw != dx::op_greater )
			return error( "E1020: Templates are limited to one parameter, line: " + dx::to_string( it->line ) );

		// Concept
		if ( (++it)->kw != dx::kw_concept )
			return error( "E1021: Templates may only be used for a concept, line: " + dx::to_string( it->line ) );

		// bool
		if ( (++it)->kw != dx::kw_bool )
			return error( "E1022: Concepts may only be booleans, line: " + dx::to_string( it->line ) );

		// Addable
		if ( (++it)->type != dx::token_identifier )
			return error( "E1023: Concepts must be named, line: " + dx::to_string( it->line ) );

		dx::String concept_name = it->value;

		if ( (++it)->kw != dx::op_assign )
			return error( "E1024: Concepts must be assigned to, line: " + dx::to_string( it->line ) );

		if ( (++it)->kw != dx::kw_requires )
			return error( "E1025: Concepts can only be assigned to a 'requires' statement, line: " + dx::to_string( it->line ) );

		if ( (++it)->kw != dx::op_roundbracket_open )
			return error( "E1026: 'requires' statement missing '(' or ')', line: " + dx::to_string( it->line ) );

		Group arguments;
		arguments.name = concept_name + "_arguments";

		++it; // Eat '('
		while( it < end && it->kw != dx::op_roundbracket_close )
		{
			if ( !parse_argument( it, end, &arguments ) )
				return false;

			if ( it->kw == dx::op_roundbracket_close )
				break;

			if ( it->kw != dx::op_roundbracket_close && it->kw != dx::op_comma )
				return error( "E1028: Syntax error in parameter list, line: " + dx::to_string( it->line ) );
			++it; // Eat comma
		}

		if ( (++it)->kw != dx::op_squigglybracket_open )
			return error( "E1029: 'requires' statement missing verification block, line: " + dx::to_string( it->line ) );

		// Concepts may only have one expression.
		if ( (++it)->type != dx::token_identifier )
			return error( "E1030: A verification block is limited to per parameter data members only, line: " + dx::to_string( it->line ) );
		
		auto left = it;

		if ( (++it)->type != dx::token_operation )
			return error( "E1031: Syntax error, line: " + dx::to_string( it->line ) );

		auto operation = it;

		if ( (++it)->type != dx::token_identifier )
			return error( "E1031: Syntax error, line: " + dx::to_string( it->line ) );

		auto right = it;
		
		if ( !arguments.get_symbol( left->value ) )
			return error( "E1032: Unknown identifier '" + left->value + "', line: " + dx::to_string( it->line ) );

		if ( !arguments.get_symbol( right->value ) )
			return error( "E1032: Unknown identifier '" + right->value + "', line: " + dx::to_string( it->line ) );

		Concept concept;
		concept.name = concept_name;
		concept.wrapper = wrapper;
		concept.requires.arguments = arguments;
		concept.requires.left = left->value;
		concept.requires.right = right->value;
		concept.requires.operation = operation->value;
		
		if ( (++it)->kw != dx::op_semicolon )
			return error( "E1005: missing semicolon, line: " + dx::to_string( it->line ) );
		
		if ( (++it)->kw != dx::op_squigglybracket_close )
			return error( "E1033: missing '}' after expression, line: " + dx::to_string( it->line ) );

		if ( (++it)->kw != dx::op_semicolon )
			return error( "E1005: missing semicolon, line: " + dx::to_string( it->line ) );

		module_.concepts.emplace_back( std::move( concept ) );
		msg( "emplaced concept " + concept.name );
	}

	return true;
}

bool Config::parse_symbol(dx::Tokenizer::iterator & it, const dx::Tokenizer::iterator & end, bool dont_semi)
{
	auto type = it++; // Type
	bool pointer = false;
	if ( it->kw == dx::op_indirect || it->kw == dx::op_mul )
		pointer = true, ++it; // eat '*'

	auto ident = it; // Identifier

	// If its not an identifier
	if ( (it++)->type != dx::token_identifier )
		return error( "E1006: expected identifier after '" + type->value + "', line: " + dx::to_string( type->line ) );

	// Eat '='
	++it;

	// Now get the value:
	auto value = it++;
	auto actual_value = end;

	if ( it->kw == dx::op_scope_resolution )
	{
		//value->value = module_.get_group( value->value )->get_symbol( it->value )->value;
		
		++it; // Eat ::
		actual_value = it++;

		if ( actual_value->type != dx::token_identifier )
			return error( "E1013: lhs of scope resolution operator can only be performed on an identifier, line: " + dx::to_string( it->line ) );
	}

	// Ok should check for semicolon
	if ( !dont_semi ) 
	{
		if ( it->kw != dx::op_semicolon )
			return error( "E1005: missing semicolon, line: " + dx::to_string( it->line ) );
		++it; // Eat semicolon
	} 
	else
		msg( "no semicolon check on line " + dx::to_string( it->line ) );

	if ( value->type == dx::token_identifier )
	{
		Symbol symbol;
		symbol.name = ident->value;

		if ( actual_value == end )
		{
			auto sym = module_.get_global_symbol( value->value );

			if ( !sym )
				return error( "E1015: '" + value->value + "' does not exist in 'GlobalScope' context, line: " + dx::to_string( it->line ) );

			symbol.value = sym->value;
			symbol.type = sym->type;
		}
		else
		{
			auto group = module_.get_group( value->value );
			if ( !group )
				return error( "E1014: Cannot use a group that has not been created, line: " + dx::to_string( it->line ) );

			auto sym = group->get_symbol( actual_value->value );

			if ( !sym )
				return error( "E1015: '" + actual_value->value + "' does not exist in '" + group->name + "' context, line: " + dx::to_string( it->line ) );
			
			symbol.value = sym->value;
			symbol.type = sym->type;
		}

		add_symbol_smart( std::move( symbol ) );
		return true;
	}


	switch( type->kw )
	{
	case dx::kw_float:
	{
		Symbol symbol;
		symbol.type = data_type_float;
		symbol.name = ident->value;
		symbol.value = value->value;
		if ( value->type != dx::token_numerical )
			return error( "E1004: Expected floating point number in float assignment, line: " + dx::to_string( ident->line ) );
		add_symbol_smart( std::move( symbol ) );
		break;
	}
	case dx::kw_int:
	{
		Symbol symbol;
		symbol.type = data_type_int;
		symbol.name = ident->value;
		symbol.value = value->value;
		if ( value->type != dx::token_numerical )
			return error( "E1003: Expected literal value in integer assignment, line: " + dx::to_string( ident->line ) );
		add_symbol_smart( std::move( symbol ) );
		break;
	}
	case dx::kw_auto:
	{
		Symbol symbol;
		symbol.name = ident->value;
		symbol.value = value->value;
		if ( value->type == dx::token_string_literal )
			symbol.type = data_type_string;
		else
			symbol.type = data_type_int;
		add_symbol_smart( std::move( symbol ) );
		break;
	}
	case dx::kw_double:
	{
		Symbol symbol;
		symbol.type = data_type_double;
		symbol.name = ident->value;
		symbol.value = value->value;
		if ( value->type != dx::token_numerical )
			return error( "E1002: Expected floating point number in double assignment, line: " + dx::to_string( ident->line ) );
		add_symbol_smart( std::move( symbol ) );
		break;
	}
	case dx::kw_char:
	{
		Symbol symbol;
		symbol.type = pointer ? data_type_string : data_type_char;
		symbol.name = ident->value;
		symbol.value = value->value;
		if ( pointer && value->type != dx::token_string_literal )
			return error( "E1001: Expected string literal in 'char*' assignment, line: " + dx::to_string( ident->line ) );
		else if ( value->type == dx::token_string_literal && !pointer )
			return error( "E1012: Cannot convert 'const char*' to 'char', line: " + dx::to_string( ident->line ) );

		add_symbol_smart( std::move( symbol ) );
		break;
	}
	default:
		return error( "E1000: Illegal type for symbol '" + ident->value + "' line: " + dx::to_string( ident->line ) );
	}
	return true;
}

bool Config::parse_symbol_concept(dx::Tokenizer::iterator & it, const dx::Tokenizer::iterator & end)
{
	auto type = it++;

	// Pointers aren't allowed
	if ( it->kw == dx::op_indirect || it->kw == dx::op_mul )
		return error( "E1034: Concept may not be created with a pointer, line: " + dx::to_string( it->line ) );

	auto ident = it++;

	// Check identifier
	if ( ident->type != dx::token_identifier )
		return error( "E1006: expected identifier after '" + type->value + "', line: " + dx::to_string( type->line ) );

	// Check '='
	if ( it->kw != dx::op_assign )
		return error( "E1035: Expected assignment operation, line: " + dx::to_string( it->line ) );

	++it; // eat '='

	auto value = it++;
	auto actual_value = end;

	// Check if assignment happens via scope res
	if ( it->kw == dx::op_scope_resolution )
	{
		++it; // Eat ::
		actual_value = it++; // Get actuall value

		if ( actual_value->type != dx::token_identifier )
			return error( "E1013: lhs of scope resolution operator can only be performed on an identifier, line: " + dx::to_string( it->line ) );
	}
	
	// Semi colon
	if ( it->kw != dx::op_semicolon )
		return error( "E1005: missing semicolon, line: " + dx::to_string( it->line ) );
	++it; // Eat semicolon

	if ( value->type == dx::token_identifier )
	{
		Symbol symbol;
		symbol.name = ident->value;

		if ( actual_value == end ) // does not use operator::
		{
			auto sym = module_.get_global_symbol( value->value );

			// Verify concept block
			if ( !verify_concept_block( type->value, sym ) )
				return false; // error created in functor

			symbol.type = sym->type;
			symbol.value = sym->value;
			add_symbol_smart( std::move( symbol ) );
			return true;
		}
		else // Uses operator::
		{
			auto group = module_.get_group( value->value );
			if ( !group )
				return error( "E1014: Cannot use a group that has not been created, line: " + dx::to_string( it->line ) );

			auto sym = group->get_symbol( actual_value->value );

			if ( !sym )
				return error( "E1015: '" + sym->value + "' does not exist in '" + group->name + "' context, line: " + dx::to_string( it->line ) );

			if ( !verify_concept_block( type->value, sym ) )
				return false;

			Symbol symbol;
			symbol.name = ident->value;
			symbol.type = sym->type;
			symbol.value = sym->value;
			add_symbol_smart( std::move( symbol ) );
			return true;
		}
	}
	return error( "E1031: Syntax error, line: " + dx::to_string( it->line ) );
}

bool Config::parse_argument(dx::Tokenizer::iterator & it, const dx::Tokenizer::iterator & end, Group *group)
{
	bool pointer = false;
	auto type = it++;
	if ( it->kw == dx::op_indirect || it->kw == dx::op_mul )
		pointer = true, ++it; // Eat '*'

	auto ident = it; // Identifier

	// If its not an identifier
	if ( (it++)->type != dx::token_identifier )
		return error( "E1006: expected identifier after '" + type->value + "', line: " + dx::to_string( type->line ) );

	Symbol symbol;
	symbol.type = is_type( type->kw ) ? (type->kw == dx::token_string_literal ? data_type_string : data_type_int) : data_type_template;
	symbol.name = ident->value;
	symbol.value = "";

	group->add_symbol( std::move( symbol ) );
	return true;
}

bool Config::is_type(dx::uint kw)
{
	return kw == dx::kw_auto || kw == dx::kw_double || kw == dx::kw_char || kw == dx::kw_int || kw == dx::kw_float;
}

bool Config::is_concept(const dx::Tokenizer::iterator & it)
{
	for ( auto &x : module_.concepts )
		if ( x.name == it->value )
			return true;
	return false;
}

bool Config::verify_concept_block(const dx::String & name, Symbol * symbol)
{
	Concept *concept = nullptr;
	for ( auto&x : module_.concepts )
		if ( x.name == name )
			concept = &x;

	if ( !concept )
		return error( "E1036: Fatal compiler error" );

	
	msg( "verifying concept block of concept '" + name + "' on symbol '" + symbol->name + "'" );
	switch( symbol->type )
	{
	case data_type_char:
	case data_type_int:
		return check_literal_op( concept->requires.operation );

	case data_type_double:
	case data_type_float:
		return check_floating_point_op( concept->requires.operation );
	
	case data_type_string:
		return check_string_op( concept->requires.operation );
	default:
		return error( "E1037: '" + symbol->name + "' has unknown type, concept on lhs cannot be deduced" );
	}

	msg( "verification block failed" );
	return false;
}

bool Config::check_literal_op(const dx::String & operation)
{
	return operation != "->" && operation != "." && operation != "->*" && operation != ".*";
}

bool Config::check_floating_point_op(const dx::String & operation)
{
	return operation != "->" && operation != "." && operation != "->*" && operation != ".*" &&
		   operation != "%" && operation != "%=";
}

bool Config::check_string_op(const dx::String & operation)
{
	return operation == "+" || operation == "+=";
}

ConfigModule Config::get_import(dx::String name)
{
	for ( auto &x : importmods_ )
		if ( x.name == name )
			return x;
	return ConfigModule();
}

dx::String Config::find_global_value(const dx::String &name)
{
	auto symbol = module_.get_global_symbol( name );
	if ( symbol )
		return symbol->value;
	return "";
}

bool Config::error(dx::String str, bool ret)
{
	module_.name = "";
	module_.concepts.clear( );
	module_.globals.clear( );
	module_.imports.clear( );
	module_.groups.clear( );
	OnError( ).Invoke( str );
	return ret;
}

bool Config::warning(dx::String str, bool ret)
{
	OnWarning( ).Invoke( str );
	return ret;
}

bool Config::msg(dx::String str, bool ret)
{
	OnMessage( ).Invoke( str );
	return ret;
}

Symbol * Group::get_symbol(const dx::String & name)
{
	for ( auto &x : symbols_ )
		if ( x.name == name )
			return &x;
	return nullptr;
}

void Group::add_symbol(Symbol symbol)
{
	symbols_.emplace_back( std::move( symbol ) );
}

dx::String Group::get_value(const dx::String & key)
{
	auto symbol = get_symbol( key );
	return symbol ? symbol->value : "";
}

Symbol * ConfigModule::get_global_symbol(const dx::String & name)
{
	for ( auto &x : globals )
		if ( x.name == name )
			return &x;

	for ( auto &x : imports )
	{
		auto sym = x.get_global_symbol( name );
		if ( sym )
			return sym;
	}
	return nullptr;
}

void ConfigModule::add_global_symbol(Symbol symbol)
{
	globals.emplace_back( std::move( symbol ) );
}

dx::String ConfigModule::get_global_value(const dx::String & key)
{
	auto symbol = get_global_symbol( key );
	return symbol ? symbol->value : "";
}

Group * ConfigModule::get_group(const dx::String & name)
{
	for ( auto&x : groups )
		if ( x.name == name )
			return &x;
	return nullptr;
}
