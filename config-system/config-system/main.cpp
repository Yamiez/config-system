#include "src\Config.h"
#include <algorithm>


int main()
{
	// Config structure:
	Config default_cfg;
	default_cfg.set_file( dx::File( true, "defaults", 
	R"(
	module defaults;
	import defines;
	int default_trigger = DEFINES_KEY_F1;
	int default_bhop = DEFINES_KEY_F2;
	int default_esp = DEFINES_KEY_F3;
	)" ) );
	default_cfg.load( );
	

	Config settings;
	settings.set_file( dx::File( false, "settings.ini", "" ) );
	settings.add_possible_import_module( *default_cfg.get_module( ) );
	settings.OnError( ) += []( dx::String msg ) { std::cout << msg << std::endl; };
	settings.OnWarning( ) += []( dx::String msg ) { std::cout << msg << std::endl; };
	settings.OnMessage( ) += []( dx::String msg ) 
	{
		if ( !msg.empty( ) )
			std::cout << msg << std::endl; 
	};
	settings.load( );

	auto module = settings.get_module( );
	
	std::cout << "Module '" << module->name << "' concepts:" << std::endl;
	for ( auto &concept : module->concepts )
	{
		std::cout << "Concept name: " << concept.name << std::endl;
		std::cout << "Concept template: " << concept.wrapper.type_name << std::endl;
		std::cout << "Requires block: " << std::endl;
		std::cout << "\tParameters: " << std::endl;;
		for ( auto &x : concept.requires.arguments.symbols_ )
			std::cout << "\t==> " << x.name << std::endl;

		std::cout << "\tVerification: '" << concept.requires.left << "' '" << concept.requires.operation << "' '" << concept.requires.right << std::endl;
	}
		
	std::cin.get( );
	return 0;
}